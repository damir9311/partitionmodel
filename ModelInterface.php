<?php

interface ModelInterface
{
    /**
     * Описывает структуру таблицы
     *
     * @return array
     */
    public static function getSchemaDefinition();
}