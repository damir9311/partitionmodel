# Эмуляция программного партиционирования
  
#### Краткое описание задачи
Создать абстрактную систему, в которой можно эмулировать партиционирование таблиц программным способом.
В реальной жизни такая задача возникла для более гибкого управления партициями.

#### Допущения
Уже есть готовая инфраструктура системы, включающая в себя код проекта, базы данных, web-серверы, системы деплоя и т.п.
В примере используются адаптированные классы, без реализации и измененной структурой кода.