<?php

/**
 * Class PartitionModel
 *
 * Имитирует программное партиционирование.
 * Объединяет несколько таблиц в единую структуру (похоже на SELECT UNION),
 * и предоставляет один интерфейс для достпупа к объединенным таблицам.
 *
 * Важно!
 * 1) Структура объединяемых таблиц должна быть одинаковой, вплоть до
 * названий всех полей и их типов.
 * 2) На текущий момент поддерживается только одно поле, по которому можно
 * партиционировать.
 * 3) Значения первичного ключа должны быть сквозными для всех таблиц этой модели.
 * Это обеспечивается при помощи дополнительной таблицы.
 */
abstract class PartitionModel extends AbstractModel implements PartitionModelInterface
{
    /**
     * @var ModelSchema[]
     */
    protected static $_partitioningSchemas = array();

    /**
     * Устанавливает объекту данные из ассоциативного массива <code>$fields</code>
     * @param array $fields ассоциативный массив полей с названиями полей в ключах массива,
     * и значениями - в значениях. Названия всех полей будут переведены в нижний регистр
     * @return bool возвращает успешность действия
     */
    public function set($fields)
    {
        $schema = static::getActualModelSchema($fields);
        foreach ($fields as $field => $value) {
            if (!empty($field)) {
                list($field, $value) = $schema->prepareToSetFieldValue($field, $value);
                if ($schema->isFieldExists($field)) {
                    $this->{$field} = $value;
                }
            }
        }
        return true;
    }

    /**
     * @inheritdoc
     * @throws Exception
     */
    public function insert()
    {
        $calledClass = get_called_class();
        if ($this->getObjectPrimaryField()) {
            throw new Exception($calledClass . " already inserted");
        }
        // затем уже вставка в нужную таблицу
        $schema = static::getActualModelSchema($this->getObjectFields());
        $seqGenerator = static::getSeqGenerator();
        $this->{$schema->getPrimaryKey()} = $seqGenerator->nextVal();
        $queryArguments = array();
        $columnList = $schema->prepareColumnList($this, $queryArguments);
        $columnValues = $schema->prepareColumnValues($this);
        $query = "INSERT INTO " . $schema->getTableName(true) . " (" .
            join(', ', $columnList) . ") VALUES (" . join(', ', $columnValues) . ")";
        $res = static::dbQuery($schema, $query, $queryArguments);

        if ($res === false) {
            throw new Exception($calledClass . " cannot be inserted");
        }
    }

    /**
     * @inheritdoc
     */
    public function update($fields)
    {
        $currentFields = $this->getObjectFields();
        // сначала найдем таблицу в которую нужно вставить запись
        $currentSchema = static::getActualModelSchema($currentFields);

        $schemaDefinition = static::getSchemaDefinition();
        if (in_array($schemaDefinition['partition_column'], array_keys($fields))) {
            $schema = static::getActualModelSchema($fields);
        } else {
            $schema = $currentSchema;
        }

        if ($currentSchema === $schema) {
            $queryArguments = array();
            $set = $currentSchema->prepareSetClause($fields, $queryArguments);
            if ($set && isset($this->{$currentSchema->getPrimaryKey()})) {
                $where = $currentSchema->prepareWhereClause(
                    array(
                        $currentSchema->getPrimaryKey() => $this->getObjectPrimaryField()
                    ),
                    $queryArguments
                );
                $query = "UPDATE " . $currentSchema->getTableName(true) . " $set $where";
                if (static::dbQuery($currentSchema, $query, $queryArguments) === false) {
                    throw new Exception(get_called_class() .
                        " cannot be updated for table " . $schema->getTableName());
                }
                return $this->set($fields);
            }
            return false;
        } else {
            // нужно перенести запись из одной таблицы в другую, если
            // поменялось значение партиционируемого поля

            // delete
            $this->delete();

            // insert
            $primaryKey = $currentSchema->getPrimaryKey();
            $fields = array_merge($currentFields, $fields);
            unset($fields[$primaryKey]);
            $this->{$primaryKey} = null;
            $this->set($fields);
            $this->insert();
            return true;
        }
    }

    /**
     * @inheritdoc
     * @throws Exception
     */
    public static function updateAll($fieldsUpdate, $fieldsWhere)
    {
        // todo: Пока не ясно как сделать оптимально.
        $schemaDefinition = static::getSchemaDefinition();

        if (in_array($schemaDefinition['partition_column'], array_keys($fieldsWhere))) {
            $whereSchema = static::getActualModelSchema($fieldsWhere);
        } else {
            $whereSchema = null;
        }
        if (in_array($schemaDefinition['partition_column'], array_keys($fieldsUpdate))) {
            $updateSchema = static::getActualModelSchema($fieldsUpdate);
        } else {
            $updateSchema = null;
        }

        // 1. $updateSchema === $whereSchema - тогда обычный updateAll
        // 2. есть $whereSchema, но нет $updateSchema - тогда тоже обычный updateAll
        if (
            ($updateSchema === $whereSchema && $updateSchema) ||
            ($whereSchema && !$updateSchema)
        ) {
            $schema = $whereSchema;
            $calledClass = get_called_class();
            $queryArguments = array();
            $set = $schema->prepareSetClause($fieldsUpdate, $queryArguments);
            $fieldsWhere = $schema->prepareWhereClause($fieldsWhere, $queryArguments);
            if (empty($fieldsWhere) && !empty($fieldsWhere)) {
                throw new Exception('Unknown fields passed to updateAll ' . $calledClass);
            }
            if ($set) {
                $query = "UPDATE " . $schema->getTableName(true) . " $set $fieldsWhere";
                if (static::dbQuery($schema, $query, $queryArguments) === false) {
                    throw new Exception($calledClass . " cannot be updated for table " .
                        $schema->getTableName());
                }
                return true;
            }
            return false;
        } elseif (
            (!$whereSchema && $updateSchema) ||
            ($updateSchema && $whereSchema && $updateSchema !== $whereSchema) ||
            (!$whereSchema && !$updateSchema)
        ) {
            // Найти записи по $fieldsWhere, сделать для них update для случаев:
            // 3. есть $updateSchema, но нет $whereSchema
            // 4. $updateSchema !== $whereSchema
            // 5. нет $whereSchema и $updateSchema
            /** @var static[] $records */
            $records = static::selectAll($fieldsWhere);
            $result = false;
            foreach ($records as $k => $record) {
                $result = $records[$k]->update($fieldsUpdate) || $result;
            }
            return $result;
        } else {
            // что-то забыли?
            throw new Exception(get_called_class() . ': An unexpected situation!');
        }
    }

    /**
     * @inheritdoc
     * @throws Exception
     */
    public function delete()
    {
        $fields = $this->getObjectFields();
        $schema = static::getActualModelSchema($fields);
        if (!$this->getObjectPrimaryField()) {
            throw new Exception(get_called_class() .
                " could not be deleted without primary key defined");
        }
        $queryArguments = array();
        $where = $schema->prepareWhereClause(
            array($schema->getPrimaryKey() => $this->getObjectPrimaryField()),
            $queryArguments
        );
        $query = "DELETE FROM " . $schema->getTableName(true) . " $where";
        if (static::dbQuery($schema, $query, $queryArguments) === false) {
            throw new Exception(get_called_class() . " cannot be deleted");
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public static function select($fields)
    {
        if (!is_array($fields)) {
            $schemas = static::schemas();
            $fields = array(
                reset($schemas)->getPrimaryKey() => $fields
            );
        }
        return parent::select($fields);
    }

    /**
     * @inheritdoc
     * @throws Exception
     */
    public static function selectLimit($fields, $limit)
    {
        $schema = static::getActualModelSchema($fields);
        if ($schema !== static::getDefaultModelSchema()) {
            $result = static::_selectLimit($schema, $fields, $limit);
        } else {
            // поискать последовательно во всех таблицах
            $result = array();
            $schemas = static::schemas();
            foreach ($schemas as $schema) {
                try {
                    $result = array_merge(
                        $result,
                        static::_selectLimit($schema, $fields, $limit)
                    );
                } catch (Exception $e) {
                    // todo: do some logging
                }
            }
        }
        return $result;
    }

    /**
     * @param ModelSchema $schema
     * @param array $fields
     * @param array|int $limit
     * @return array
     * @throws Exception
     */
    private static function _selectLimit($schema, $fields, $limit)
    {
        $calledClass = get_called_class();
        $queryArguments = array();
        $selectWhere = $schema->prepareWhereClause($fields, $queryArguments);
        //если переданы не пустые поля для поиска объекта, но ни одно из них
        //не подходит (сформированое пустое условие WHERE), то надо выдать ошибку
        if (empty($selectWhere) && !empty($fields)) {
            throw new Exception('Unknown fields passed to select ' . $calledClass);
        }
        $selectLimit = $schema->prepareLimitClause($limit, $queryArguments);
        $query = $schema->prepareSelect('*', $selectWhere, $selectLimit);
        $res = static::dbQuery($schema, $query, $queryArguments);
        if ($res === false) {
            throw new Exception("Could not find " . $calledClass);
        }
        return static::instantiateObjects($res);
    }

    /**
     * @inheritdoc
     */
    public static function provide($fields)
    {
        /** @var static $object */
        $object = static::select($fields);
        if (!$object) {
            $reflection = new ReflectionClass(get_called_class());
            $object = $reflection->newInstance($fields);
            $object->insert();
        }
        return $object;
    }

    /**
     * @inheritdoc
     */
    public static function deleteAll($fields)
    {
        $schema = static::getActualModelSchema($fields);
        if ($schema !== static::getDefaultModelSchema()) {
            $result = static::_deleteAll($schema, $fields);
        } else {
            $result = false;
            $schemas = static::schemas();
            foreach ($schemas as $schema) {
                try {
                    $result = static::_deleteAll($schema, $fields) || $result;
                } catch (Exception $e) {
                    // todo: do some logging
                }
            }
        }
        return $result;
    }

    /**
     * @param ModelSchema $schema
     * @param array $fields
     * @return bool
     * @throws Exception
     */
    private static function _deleteAll($schema, $fields)
    {
        $calledClass = get_called_class();
        $queryArguments = array();
        $selectWhere = $schema->prepareWhereClause($fields, $queryArguments);
        if (empty($selectWhere) && !empty($fields)) {
            throw new Exception('Unknown fields passed to deleteAll ' . $calledClass);
        }
        $query = "DELETE FROM " . $schema->getTableName(true) . " $selectWhere";
        if (static::dbQuery($schema, $query, $queryArguments) === false) {
            throw new Exception($calledClass . " cannot be deleted");
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public static function selectFields($fieldsSelect, $fieldsWhere)
    {
        $schema = static::getActualModelSchema($fieldsWhere);
        if ($schema !== static::getDefaultModelSchema()) {
            $result = static::_selectFields($schema, $fieldsSelect, $fieldsWhere);
        } else {
            $selectResult = null;
            $schemas = static::schemas();
            foreach ($schemas as $schema) {
                try {
                    $r = static::_selectFields($schema, $fieldsSelect, $fieldsWhere);
                    if ($r) {
                        $selectResult[] = $r;
                    }
                } catch (Exception $e) {
                    // todo: do some logging
                }
            }
            // теперь нужно доагрегировать
            if ($selectResult) {
                $result = array();
                foreach ($fieldsSelect as $function => $field) {
                    $function = strtolower($function);
                    if (in_array($function, static::$_aggregationFunctions)) {
                        switch ($function) {
                            case 'sum':
                            case 'count':
                                $result[$function] = array_reduce(
                                    $selectResult,
                                    function ($carry, $item) use ($function) {
                                        $carry += $item[$function];
                                        return $carry;
                                    }
                                );
                                break;
                            case 'max':
                            case 'min':
                                $result[$function] = array_reduce(
                                    $selectResult,
                                    function ($carry, $item) use ($function) {
                                        if ($function == 'max') {
                                            $condition = $item[$function] > $carry;
                                        } else {
                                            $condition = $item[$function] < $carry;
                                        }
                                        $carry = $condition ? $item[$function] : $carry;
                                        return $carry;
                                    },
                                    0
                                );
                                break;
                            default:
                                throw new Exception(
                                    get_called_class() .
                                    '::selectFields: aggregation function not implemented'
                                );
                        }
                    } else {
                        $result[$field] = $selectResult[0][$field];
                    }
                }
            } else {
                $result = null;
            }
        }
        return $result;
    }

    /**
     * @param ModelSchema $schema
     * @param array $fieldsSelect
     * @param array $fieldsWhere
     * @return array|bool|null
     * @throws Exception
     */
    private static function _selectFields($schema, $fieldsSelect, $fieldsWhere)
    {
        $calledClass = get_called_class();
        $queryArguments = array();
        $selectWhere = $schema->prepareWhereClause($fieldsWhere, $queryArguments);
        // если переданы не пустые поля для поиска объекта, но ни одно из них
        // не подходит (сформированое пустое условие WHERE), то надо выдать ошибку
        if (empty($selectWhere) && !empty($fieldsWhere)) {
            throw new Exception(
                'Unknown fields for where passed to select ' . $calledClass);
        }
        $columns = array();
        foreach ($fieldsSelect as $function => $field) {
            if ($schema->isFieldExists($field)) {
                $field = $schema->prepareColumn($field);
                $function = strtolower($function);
                if (in_array($function, static::$_aggregationFunctions)) {
                    $field = "$function($field) as '$function'";
                }
                $columns[] = $field;
            }
        }
        if (empty($columns)) {
            throw new Exception('Unknown fields specified to select ' . $calledClass);
        }
        $query = "SELECT " . join(', ', $columns) . " FROM " .
            $schema->getTableName(true) . " $selectWhere";
        $res = static::dbQuery($schema, $query, $queryArguments);
        if ($res === false) {
            throw new Exception("Could not selectFields of " . $calledClass);
        }
        return db_fetch_array($res);
    }

    /**
     * @inheritdoc
     */
    public static function selectField($field, $primaryKey)
    {
        $schema = static::getActualModelSchema($field);
        if (!$schema->isFieldExists($field)) {
            throw new Exception("Unknown field name");
        }
        $result = static::selectFields(
            array($field),
            array($schema->getPrimaryKey() => $primaryKey)
        );
        return isset($result[$field]) ? $result[$field] : null;
    }

    ///////// новые методы /////////////////////////////////////////////////////

    /**
     * Возвращает схемы данной модели
     * @return ModelSchema[]
     */
    public static function schemas()
    {
        $modelCalled = get_called_class();
        if (!array_key_exists($modelCalled, static::$_partitioningSchemas)) {

            $schemaDefinition = static::getSchemaDefinition();
            $schema = array(
                'fields' => $schemaDefinition['fields'],
                'primary_key' => $schemaDefinition['primary_key'],
            );
            if (isset($schemaDefinition['connection_name'])) {
                $schema['connection_name'] = $schemaDefinition['connection_name'];
            }

            // формируем схемы по основным таблицам
            foreach ($schemaDefinition['tables'] as $table) {
                $schema['table_name'] = $table['table_name'];
                static::$_partitioningSchemas[$modelCalled][$table['table_name']] =
                    new ModelSchema($schema);
            }

            // теперь формируем схему для базовой таблицы
            // эта проверка нужна, т.к. базовой таблицей может оказаться одна из
            // таблиц из списка tables
            if (!isset(static::$_partitioningSchemas[$modelCalled][$schemaDefinition['base_table']])) {
                $schema['table_name'] = $schemaDefinition['base_table'];
                static::$_partitioningSchemas[$modelCalled][$schemaDefinition['base_table']] =
                    new ModelSchema($schema);
            }
        }
        return static::$_partitioningSchemas[$modelCalled];
    }

    /**
     * По переданным полям определяет актуальную схему модели, если не находит,
     * то возвращает схему по умолчанию.
     *
     * @param array $fields
     * @return ModelSchema
     */
    public static function getActualModelSchema($fields)
    {
        $currentTable = null;
        $schemaDefinition = static::getSchemaDefinition();
        foreach ($schemaDefinition['fields'] as $fieldName => $field) {
            if (isset($fields[$fieldName]) && $fieldName == $schemaDefinition['partition_column']) {
                foreach ($schemaDefinition['tables'] as $table) {
                    if ($fields[$fieldName] == $table['partition_value']) {
                        $currentTable = $table['table_name'];
                        break;
                    }
                }
                break;
            }
        }
        if (!$currentTable) {
            $currentTable = $schemaDefinition['base_table'];
        }

        $schemas = static::schemas();
        return $schemas[$currentTable];
    }

    /**
     * Возвращает схему для базовой таблицы.
     * @return ModelSchema
     */
    protected static function getDefaultModelSchema()
    {
        $schemaDefinition = static::getSchemaDefinition();
        $schemas = static::schemas();
        return $schemas[$schemaDefinition['base_table']];
    }

    /**
     * Возвращает первое попавшееся имя колонки таблицы не являющейся первичным
     * ключем или партиционируемым
     * @param ModelSchema $schema
     * @return string
     * @throws Exception
     */
    protected function getAnyColumnName(ModelSchema $schema)
    {
        $fields = $schema->getFieldDefinitions();
        $primaryColumnName = $schema->getPrimaryKey();
        $schemaDefinition = static::getSchemaDefinition();
        $anyColumnName = '';
        $partitionColumnName = $schemaDefinition['partition_column'];

        foreach ($fields as $field) {
            if (!in_array($field, array($partitionColumnName, $primaryColumnName))) {
                $anyColumnName = $field;
            }
        }

        if (!$anyColumnName) {
            throw new Exception('No value for getAnyColumnName()!');
        }

        return $anyColumnName;
    }

    /**
     * Получение генератора последовательностей для текущей модели
     * @return SequenceGenerator
     */
    protected static function getSeqGenerator()
    {
        $schemaDefinition = static::getSchemaDefinition();
        $sequenceName = $schemaDefinition['sequence_pk_name'];
        $seqGenerator = SequenceGenerator::getSequence($sequenceName);

        return $seqGenerator;
    }

    ///////// все что скопировал из Model //////////////////////////////////////

    /**
     * Возвращает название первичного ключа таблицы
     *
     * @return null|string
     */
    public function getObjectPrimaryField() {}

    /**
     * Возвращает ассоциативный массив полей и их значений,
     * которые есть в данном объекте
     * @return array
     */
    public function getObjectFields() {}

    /**
     * Выполняет непосредственно sql-запрос в БД
     *
     * @param ModelSchema|null $schema
     * @param $query
     * @param $queryArguments
     *
     * @return mixed
     */
    public static function dbQuery($schema, $query, $queryArguments) {}

    /**
     * Возвращает название подключения, используемое данной моделью
     * @param ModelSchema $schema
     * @return string|null
     */
    public static function getConnectionName($schema) {}

    /**
     * Находит соединение БД для указанной таблицы
     *
     * @param string $table
     *
     * @return string|null
     * @throws Exception
     */
    public static function getTableConnection($table) {}

    /**
     * Рекурсивно ищет соединение для таблицы
     *
     * @param $table
     * @param $connections
     *
     * @return string|null
     * @throws Exception
     */
    static function dbConnectionSearch($table, $connections) {}

    /**
     * @param $dbQueryResult
     * @return static[]
     */
    public static function instantiateObjects($dbQueryResult) {}
}