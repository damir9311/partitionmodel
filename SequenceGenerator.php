<?php

/**
 * Class SequenceGenerator
 *
 * Счетчик на хранимых процедурах
 */
class SequenceGenerator extends Model
{
    /**
     * @var int
     */
    public $sequence_id;

    /**
     * @var string
     */
    public $sequence_name = null;

    /**
     * @var int
     */
    public $sequence_increment = 1;

    /**
     * @var int
     */
    public $sequence_min_value = 1;

    /**
     * @var int
     */
    public $sequence_max_value = 9223372036854775807;

    /**
     * @var int
     */
    public $sequence_cur_value = null;

    /**
     * @var bool
     */
    public $sequence_cycle;

    public static function getSchemaDefinition()
    {
        return array(
            'table_name' => 'sequence',
            'primary_key' => 'sequence_id',
            'fields' => array(
                'sequence_id' => array('db_type' => '%d'),
                'sequence_name' => array('db_type' => '%s'),
                'sequence_increment' => array('db_type' => '%d'),
                'sequence_min_value' => array('db_type' => '%d'),
                'sequence_max_value' => array('db_type' => '%d'),
                'sequence_cur_value' => array('db_type' => '%d'),
                'sequence_cycle' => array('db_type' => '%b'),
            )
        );
    }

    /**
     * Получение следующего значения счетчика
     * @return int новое значение счетчика
     * @throws Exception
     */
    public function nextVal()
    {
        if (!$this->sequence_name) {
            throw new Exception('There is no name of sequence');
        }
        $query = "SELECT nextval('%s') as next_sequence";
        $queryArguments = array($this->sequence_name);
        $dbConnection = DbConnection::getInstance();
        $res = $dbConnection->dbQuery($this->getConnectionName(), $query, $queryArguments);
        $calledClass = get_called_class();
        if ($res === false) {
            throw new Exception($calledClass . " cant get next val of sequence");
        }
        $row = db_fetch_array($res);
        if (!isset($row['next_sequence'])) {
            throw new Exception($calledClass . " there is no sequence {$this->sequence_name}");
        }
        $this->sequence_cur_value = $row['next_sequence'];

        return $this->sequence_cur_value;
    }

    /**
     * Сброс счетчика
     * @throws Exception
     */
    public function reset()
    {
        if (!$this->sequence_name) {
            throw new Exception('There is no name of sequence');
        }
        $this->sequence_cur_value = $this->sequence_min_value;
        parent::update(array(
            'sequence_cur_value' => $this->sequence_min_value
        ));
    }

    /**
     * Получение счетчика
     * @param $sequenceName
     * @throws Exception
     * @return SequenceGenerator
     */
    public static function getSequence($sequenceName)
    {
        $sequenceGenerator = parent::select(array(
            'sequence_name' => $sequenceName
        ));

        if (!$sequenceGenerator) {
            $called = get_called_class();
            throw new Exception('No sequence generator for model ' . $called);
        }

        return $sequenceGenerator;
    }

    // Геттеры

    /**
     * @return string
     */
    public function getSequenceName()
    {
        return $this->sequence_name;
    }

    /**
     * @return int
     */
    public function getSequenceIncrement()
    {
        return $this->sequence_increment;
    }

    /**
     * @return int
     */
    public function getSequenceMinValue()
    {
        return $this->sequence_min_value;
    }

    /**
     * @return int
     */
    public function getSequenceMaxValue()
    {
        return $this->sequence_max_value;
    }

    /**
     * @return boolean
     */
    public function isSequenceCycle()
    {
        return $this->sequence_cycle;
    }

    //Заглушки блокирующие прямой доступ к счетчикам

    public function insert()
    {
        throw new Exception('This method cannot be used');
    }

    public function update($fields)
    {
        throw new Exception('This method cannot be used');
    }

    public static function select($fields)
    {
        throw new Exception('This method cannot be used');
    }

    public static function updateAll($fields_update, $fields_where)
    {
        throw new Exception('This method cannot be used');
    }

    public static function deleteAll($fields)
    {
        throw new Exception('This method cannot be used');
    }

    public static function selectFields($fields_select, $fields_where)
    {
        throw new Exception('This method cannot be used');
    }

    public static function selectField($field, $primary_key)
    {
        throw new Exception('This method cannot be used');
    }

    public static function lastInsertId()
    {
        throw new Exception('This method cannot be used');
    }

    public static function insertAll($fieldNames, $fieldValuesList)
    {
        throw new Exception('This method cannot be used');
    }
}