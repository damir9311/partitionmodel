<?php

interface AbstractModelInterface
{
    /**
     * Создает запись в БД.
     */
    public function insert();

    /**
     * Обновляет данные объекта, записывает изменения в БД
     *
     * @param array $fields ассоциативный массив полей и их значений
     * @return bool
     * @throws Exception
     */
    public function update($fields);

    /**
     * Массово обновляет поля таблицы $fieldsUpdate по условию $fieldsWhere
     *
     * @param $fieldsUpdate
     * @param $fieldsWhere
     * @return bool
     */
    public static function updateAll($fieldsUpdate, $fieldsWhere);

    /**
     * Устанавливает объекту данные из ассоциативного массива $fields
     * @param array $fields ассоциативный массив полей с названиями полей в ключах массива,
     * и значениями - в значениях. Названия всех полей будут переведены в нижний регистр
     * @return bool - успешность операции
     */
    public function set($fields);

    /**
     * Удаляет запись из БД
     */
    public function delete();

    /**
     * Ищет записи в базе данных по набору параметров $fields
     * в количестве первых $limit штук
     * @param array $fields ассоциативный массив параметров,
     * все названия полей будут приведены в нижний регистр.
     * Если в качестве значения параметра передан массив, то будет
     * произведен поиск по параметрам с любым из этих значений
     * (соединение с помощью ИЛИ)
     * @param int|array $limit количество требуемых объектов, можно задать 0, тогда
     * будут возвращены все объекты, можно задать массив, тогда первое значение покажет
     * с какого объекта нужно отдавать, а второе - сколько объектов (LIMIT в SQL)
     * @return array массив найденных объектов
     *
     * @param array $fields
     * @param array|int $limit
     * @return static[]
     */
    public static function selectLimit($fields, $limit);

    /**
     * Возвращает существующую запись с полями $fields, а если не находит такой - создает
     * @param array $fields ассоциативный массив параметров объекта
     * @return static
     */
    public static function provide($fields);

    /**
     * Удаляет все записи найденные по параметрам
     * @param array $fields ассоциативный массив параметров для поиска
     * @return true если объекты найдены и удалены, false - если таких объектов не было
     */
    public static function deleteAll($fields);

    /**
     * Выбирает определенные поля таблицы
     * @param array $fieldsSelect - список полей 
     * @param array $fieldsWhere - условия выборки
     * @return mixed
     */
    public static function selectFields($fieldsSelect, $fieldsWhere);

    /**
     * Возвращает значение поля $field из записи, заданной значением первичного ключа $primary_key
     * Поиск происходит без загрузки всего объекта
     * @param string $field необходимое поле
     * @param mixed $primaryKey значение первичного ключа для поиска записи
     * @throws Exception
     * @return mixed значение поля или NULL, если запись не будет найдена
     */
    public static function selectField($field, $primaryKey);
}