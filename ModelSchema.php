<?php

/**
 * Class ModelSchema
 *
 * Схема модели, представление модели в базе данных.
 * Также схема помогает готовить sql-запросы
 */
class ModelSchema
{

    /**
     * Описание схемы. Формат:
     * array(
     *   'table_name' => '<название таблицы>',
     *   'primary_key' => '<название колонки первичного ключа
     *   'connection_name' => '<название соединения, необязательный параметр,
     *                          берется в случае, если не указан в конфиге в
     *                          параметре map_table_connections для таблицы>',
     *   'fields' => array(
     *     '<название колонки (только нижний регистр)>' => array(
     *       'db_type' => '<тип(%d, %s, %n, %f, %b)>',
     *       'class' => '<класс-обработчик в php для этого поля в модели>'
     *     )
     *   )
     * )
     * @var array
     */
    protected $_schema;

    protected static $_comparisonSigns = array('>', '>=', '=', '<>', '<=', '<');

    function __construct($schemaDefinition)
    {
        $this->_schema = $schemaDefinition;
    }

    public function getTableName($isSql = false) {}

    public function getFieldNames() {}

    public function getFieldType($fieldName) {}

    public function getFieldDbType($fieldName) {}

    /**
     * @return array
     */
    public function getFieldDefinitions() {}

    public function getFieldDefinition($fieldName) {}

    /**
     * @return string
     */
    public function getPrimaryKey() {}

    public function getConnectionName() {}

    /**
     * @param $fieldName
     * @return string
     */
    public function prepareColumn($fieldName) {}

    public function prepareComparison($comparison, $fieldName) {}

    public function prepareEquality($fieldName) {}

    /**
     * Подготавливает поле и значение для установки объекту
     * @param string $field название поля
     * @param string $value значение поля
     * @return array массив из двух значений - подготовленное название поля,
     * подготовленное значение поля.
     * Удобно примнимать с помощью list($field, $value) = ..
     */
    public function prepareToSetFieldValue($field, $value) {}

    /**
     * Подготавливает поле и значение для использования в SQL-запросе
     * @param string $field название поля
     * @param string $value значение поля
     * @return array массив из двух значений - подготовленное название поля,
     * подготовленное значение поля.
     * Удобно примнимать с помощью list($field, $value) = ..
     */
    public function prepareToSqlFieldValue($field, $value) {}

    /**
     * Подготавливает плейсхолдер для параметра SQL-запроса с названием $field.
     * Подготовка заключается в том, ставить кавычки вокруг типа данных БД или нет.
     * @param string $field
     * @return string
     */
    public function prepareFieldDbType($field) {}

    /**
     * Собирает строку запроса SELECT
     * @param string $field поле, которое необходимо получить из таблицы
     * (не проверяется на существование и валидность), можно указать '*',
     * если требуется получить все поля.
     * @param string $where часть запроса с WHERE
     * @param string $limit часть запроса с LIMIT
     * @return string строка запроса
     */
    public function prepareSelect($field = '*', $where = '', $limit = '') {}

    /**
     * @param $object
     * @param $queryArguments
     * @return array
     */
    public function prepareColumnList($object, &$queryArguments) {}

    /**
     * @param $object
     * @return array
     */
    public function prepareColumnValues($object) {}

    /**
     * @param $fields
     * @param $queryArguments
     * @return string
     */
    public function prepareSetClause($fields, &$queryArguments) {}

    /**
     * Формирует часть LIMIT SQL-запроса, при этом добавляет необходимые параметры в
     * $query_arguments
     * @param int|array $limit либо число, либо массив из двух чисел (для формирования LIMIT)
     * @param array $queryArguments массив аргументов для передачи в массив
     * @return string часть SQL-запроса с LIMIT
     */
    public function prepareLimitClause($limit, &$queryArguments) {}

    /**
     * По набору параметров $fields формирует условие WHERE для SQL-запроса, при этом добавляет
     * необходимые параметры в $query_arguments
     * @param array $fields ассоциативный массив параметров, где ключи - колонки, а значения -
     * значения, либо массив значений (соединяются с помощью OR)
     * @param array $queryArguments массив параметров для передачи в запрос
     * @return string условие WHERE для SQL-запроса; содержит слово "WHERE", если есть условие,
     * или равно пустой строке, если нечего сравнивать
     */
    public function prepareWhereClause($fields, &$queryArguments) {}

    /**
     * @param $fieldName
     * @return bool
     */
    public function isFieldExists($fieldName) {}

    /**
     * Фильтрует массив полей и их значений, оставляя только те,
     * которые есть в данной модели
     * @param array $fields ассоциативный массив поле => значение
     * @return array ассоциативный массив поле => значение
     */
    public function filterFields($fields) {}

    /**
     * Формирует часть запроса IN
     * @param $field
     * @param $values
     * @return string
     */
    public function prepareIn($field, $values) {}
}