<?php

abstract class AbstractModel implements AbstractModelInterface
{
    protected static $_aggregationFunctions = array('sum', 'count', 'max', 'min');

    function __construct($fields)
    {
        $this->set($fields);
    }

    /**
     * Ищет первую запись в базе данных по набору параметров
     * @param mixed $fields значение первичного ключа, по которому
     * надо произвести поиск или ассоциативный массив параметров,
     * все названия полей будут приведены в нижний регистр,
     * Если в качестве значения параметра передан массив, то будет
     * произведен поиск по параметрам с любым из этих значений
     * (соединение с помощью ИЛИ)
     * @return stdClass найденный объект, или null
     */
    public static function select($fields)
    {
        $objects = static::selectLimit($fields, 1);
        if (!empty($objects)) {
            return $objects[0];
        }
        return null;
    }

    /**
     * Ищет все записи в базе данных по набору параметров
     * @param array $fields ассоциативный массив параметров,
     * все названия полей будут приведены в нижний регистр.
     * Если в качестве значения параметра передан массив, то будет
     * произведен поиск по параметрам с любым из этих значений
     * (соединение с помощью ИЛИ)
     * @return static[] возвращает массив найденных объектов
     */
    public static function selectAll($fields = array())
    {
        return static::selectLimit($fields, 0);
    }
}