<?php

/**
 * Представляет основные методы по получению и записи объектов в БД
 */
abstract class Model extends AbstractModel implements ModelInterface
{
    protected static $_schemas = array();

    /**
     * Возвращает схему данной модели
     * @return ModelSchema
     */
    public static function schema() {}

    /**
     * Создает запись в БД.
     * При этом, если у объекта уже назначено ключевое поле, то выбрасывается
     * исключение, потому что считается, что объект уже был вставлен до этого.
     * После вставки объекта ключевому полю назначется соответствующее
     * автоинкрементированное значение из таблицы.
     */
    public function insert() {}

    /**
     * @inheritdoc
     */
    public function update($fields) {}

    /**
     * @inheritdoc
     */
    public static function select($fields) {}

    /**
     * @inheritdoc
     */
    public function set($fields) {}

    /**
     * @inheritdoc
     */
    public function delete() {}

    /**
     * @inheritdoc
     */
    public static function selectLimit($fields, $limit) {}

    /**
     * @inheritdoc
     */
    public static function instantiateObjects($dbQueryResult) {}

    /**
     * @inheritdoc
     */
    public static function provide($fields) {}

    /**
     * @inheritdoc
     */
    public static function updateAll($fields_update, $fields_where) {}

    /**
     * @inheritdoc
     */
    public static function deleteAll($fields) {}

    /**
     * @inheritdoc
     */
    public static function selectFields($fields_select, $fields_where) {}

    /**
     * @inheritdoc
     */
    public static function selectField($field, $primary_key) {}

    /**
     * @inheritdoc
     */
    public function getObjectPrimaryField() {}

    /**
     * Возвращает ассоциативный массив полей и их значений,
     * которые есть в данном объекте
     * @return array
     */
    public function getObjectFields() {}

    /**
     * Рекурсивно ищет соединение для таблицы
     *
     * @param $table
     * @param $connections
     *
     * @return string|null
     * @throws Exception
     */
    static function dbConnectionSearch($table, $connections) {}

    /**
     * Выполняет непосредственно sql-запрос в БД
     *
     * @param $query
     *
     * @return mixed
     */
    public static function dbQuery($query) {}

    /**
     * Возвращает название подключения, используемое данной моделью
     * @return string|null
     */
    public static function getConnectionName() {}

    /**
     * Находит соединение БД для указанной таблицы
     *
     * @param $table |string
     *
     * @return string|null
     * @throws Exception
     */
    public static function getTableConnection($table) {}

    /**
     * Возвращает последний идентификатор вставленной записи в таблицу
     * @return mixed
     */
    public static function lastInsertId() {}

    /**
     * Возвращает количество затронутых записей при последнем запросе в БД.
     * Следует обратить внимание, что affected_rows не привязывается к конкретной таблице,
     * поэтому будет возвращено значение, привязанное только к конкретному подключению к БД.
     * @return int
     */
    public static function affectedRows() {}

    /**
     * Добавляет в базу несколько записей с полями,
     * названия которых перечислены в $fieldNames, а значения в $fieldValuesList
     * @param array $fieldNames названия полей, которые используются для вставки новых записей
     * @param array $fieldValuesList список наборов значений, которые будут вставлены. Формат:
     * array(
     *   array('field1_value1', 'field2_value1', ...),
     *   array('field2_value1', 'field2_value2', ...),
     *   ...
     * )
     * @return bool
     * @throws Exception
     */
    public static function insertAll($fieldNames, $fieldValuesList) {}

    /**
     * Подготавливает данные для вставки в таблицу
     * 
     * @param $fieldNames
     * @param $fieldValuesList
     */
    protected static function prepareInsertAll($fieldNames, $fieldValuesList) {}

    /**
     * Возвращает список полей $fieldName из $o
     * @param array|int|object $o объект, содержащий поле $fieldName, значение для $fieldName,
     * массив объектов, содержащих поле $fieldName или массив значений для $fieldName
     * @param string $fieldName название поля
     * @return array
     */
    public static function getListOfFields($o, $fieldName) {}
}